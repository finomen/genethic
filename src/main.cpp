#include "global.h"
#include "algorithm.h"
#include "problem/ant3/model.h"
#include "automaton/moore/full_table.h"
#include "automaton/moore/reduced_table.h"
#include <boost/functional/factory.hpp>
#include <boost/make_shared.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include "settings/settings_manager.h"
#include <vector>
boost::shared_ptr<automaton::representation> ft()
{
    return boost::make_shared<automaton::moore::full_table>();
}

boost::shared_ptr<automaton::representation> rt()
{
    return boost::make_shared<automaton::moore::reduced_table>();
}

int main(int argc, const char * argv[])
{
    for (size_t i = 1; i < argc; ++i)
    {
        typedef std::vector<std::string> split_vector_type;
        split_vector_type SplitVec;
        std::string s(argv[i]);
        boost::algorithm::split(SplitVec, s, boost::algorithm::is_any_of(" ="), boost::algorithm::token_compress_on );
        settings::settings_manager::instance().override(SplitVec[0], SplitVec.back());
    }

    std::cout << settings::settings_manager::instance().data() << std::endl;
    std::fstream out("settings.txt", std::ios::out);
    out << settings::settings_manager::instance().data() << std::endl;
    out.close();
    boost::shared_ptr<problem::ant3::model> m = boost::make_shared<problem::ant3::model>();
    algorithm f(boost::bind(&ft), m);
    f.run("full_table");
    algorithm r(boost::bind(&rt), m);
    r.run("reduced_table");
    return 0;
}
