/*
 * reduced_table.cpp
 *
 *  Created on: Oct 14, 2011
 *      Author: Filchenko Nikolay
 */

#include "automaton/moore/reduced_table.h"

#include "settings/settings_manager.h"
#include <iostream>
#include <fstream>
#include "global.h"

#include <stack>

SM_ADD_PARAM(size_t, rt_automaton_size, 20);
SM_ADD_PARAM(size_t, rt_x_size, 8);
SM_ADD_PARAM(size_t, rt_r_x_size, 3);
SM_ADD_PARAM(size_t, rt_z_count, 3);
SM_ADD_PARAM(bool, rt_one_point_crossover, true);
SM_ADD_PARAM(bool, rt_mutate_count, 0);
SM_ADD_PARAM(double, rt_mutate_p, 0.15);



namespace automaton {
namespace moore {

reduced_table::reduced_table()
{
    size_t sz = SM_PARAM(size_t, rt_automaton_size);
    size_t xs = SM_PARAM(size_t, rt_x_size);
    size_t rxs = SM_PARAM(size_t, rt_r_x_size);
    size_t zc = SM_PARAM(size_t, rt_z_count);

    for (size_t i = 0; i < sz; ++i)
    {
        a.push_back(std::pair<size_t, std::pair<std::vector<size_t>, size_t > >(std::make_pair(0, std::make_pair(std::vector<size_t>(1 << xs), 0))));

        a[i].first = random(zc);
        
        a[i].second.second = 0;
        
        for (size_t j = 0; j < rxs; ++j)
        {
            size_t p = 0;
            while ( (1 << (p = random(xs))) & a[i].second.second );
            a[i].second.second = a[i].second.second | (1 << p);
            
        }

        for (size_t j = 0; j < (1 << xs); ++j)
        {
            a[i].second.first[j] = random(sz);
        }

    }
}

boost::shared_ptr<individual> reduced_table::mutate() const
{
    return boost::shared_ptr<reduced_table>(new reduced_table(this));
}

reduced_table::reduced_table(const reduced_table * o)
{
    a = o->a;
    size_t mc = SM_PARAM(size_t, rt_mutate_count);
    double mp = SM_PARAM(double, rt_mutate_p);
    size_t sz = SM_PARAM(size_t, rt_automaton_size);
    size_t xs = SM_PARAM(size_t, rt_x_size);
    size_t rxs = SM_PARAM(size_t, rt_r_x_size);
    size_t zc = SM_PARAM(size_t, rt_z_count);

    if (mc)
    {
        std::vector<bool> mut(sz, false);
        size_t c = 0;

        while (c < mc)
        {
            size_t mr = random(sz);

            if (!mut[mr])
            {
                mut[mr] = true;
                ++c;
            }
        }

        for (size_t j = 0; j < sz; ++j)
        {
            if (random(0.5))
            {
                size_t f = 0;
                while (a[j].second.second & (1 << (f = random(xs))));
                a[j].second.second = a[j].second.second | (1 << f);
                
                while ((a[j].second.second & (1 << (f = random(xs)))) == 0);
                a[j].second.second = a[j].second.second ^ (1 << f);
            }
            if (mut[j])
            {
                if (random(0.5))
                {
                    a[j].first = random(zc);
                }
                else
                {
                    for (size_t i = 0; i < (1 << rxs); ++i)
                    {
                        if (random(mp))
                        {
                            a[j].second.first[i] = random(sz);
                        }
                    }
                }
            }
        }        
    }
    else
    {
        for (size_t j = 0; j < sz; ++j)
        {
            if (random(0.5))
            {
                size_t f = 0;
                while (a[j].second.second & (1 << (f = random(xs))));
                a[j].second.second = a[j].second.second | (1 << f);
                
                while ((a[j].second.second & (1 << (f = random(xs)))) == 0);
                a[j].second.second = a[j].second.second ^ (1 << f);
            }
            
            if (random(mp))
            {
                if (random(0.5))
                {
                    a[j].first = random(zc);
                }
                else
                {
                    for (size_t i = 0; i < (1 << rxs); ++i)
                    {
                        if (random(0.5))
                        {
                            a[j].second.first[i] = random(sz);
                        }
                    }
                }
            }
        }
    }
}

reduced_table::reduced_table(std::vector<std::pair<size_t, std::pair<std::vector<size_t>, size_t> > > const & a_) :
    a(a_)
{
}

std::pair<boost::shared_ptr<individual>, boost::shared_ptr<individual> > reduced_table::corssover(boost::shared_ptr<individual> const & o) const
{
    boost::shared_ptr<reduced_table> another = boost::dynamic_pointer_cast<reduced_table>(o);

    std::vector<std::pair<size_t, std::pair<std::vector<size_t>, size_t> > > a1, a2;
    size_t sz = SM_PARAM(size_t, rt_automaton_size);

    std::vector<size_t> an1(0), an2(0), rn1(sz), rn2(sz);

    std::stack<size_t, std::vector<size_t> > st;
    std::vector<bool> mark(sz, false);
    
    for (size_t i = 0; i < sz; ++i)
    {
        if (mark[i])
            continue;
        st.push(i);
        mark[i] = true;
        while (!st.empty())
        {
            size_t u = st.top();
            st.pop();
            rn1[u] = an1.size();
            an1.push_back(u);

            for (size_t i = 0; i < a[u].second.first.size(); ++i)
            {
                size_t v = a[u].second.first[i];
                if (!mark[v])
                {
                    st.push(v);
                    mark[v] = true;
                }
            }
        }
    }
    mark.assign(sz, false);
    for (size_t i = 0; i < sz; ++i)
    {
        if (mark[i])
            continue;
        st.push(i);
        mark[i] = true;
        while (!st.empty())
        {
            size_t u = st.top();
            st.pop();
            rn2[u] = an2.size();
            an2.push_back(u);

            for (size_t i = 0; i < another->a[u].second.first.size(); ++i)
            {
                size_t v = another->a[u].second.first[i];
                if (!mark[v])
                {
                    st.push(v);
                    mark[v] = true;
                }
            }
        }
    }
    bool opc = SM_PARAM(bool, rt_one_point_crossover);
    size_t xs = SM_PARAM(size_t, rt_x_size);
    size_t rxs = SM_PARAM(size_t, rt_r_x_size);
    
    for (size_t i = 0; i < sz; ++i)
    {
        size_t fp = opc ? 0 : random(1 << rxs);
        size_t sp = random(1 << rxs);

        std::vector<size_t> c1, c2;

        if (fp > sp)
            std::swap(fp, sp);

        for (size_t j = 0; j < fp; ++j)
        {
            c1.push_back(rn1[a[an1[i]].second.first[j]]);
            c2.push_back(rn2[another->a[an2[i]].second.first[j]]);
        }

        for (size_t j = fp; j < sp; ++j)
        {
            c2.push_back(rn1[a[an1[i]].second.first[j]]);
            c1.push_back(rn2[another->a[an2[i]].second.first[j]]);
        }

        for (size_t j = sp; j < (1 << rxs); ++j)
        {
            c1.push_back(rn1[a[an1[i]].second.first[j]]);
            c2.push_back(rn2[another->a[an2[i]].second.first[j]]);
        }

        size_t s1 = a[an1[i]].first, s2 = another->a[an2[i]].first;

        if (random(0.5))
            std::swap(s1, s2);
        size_t mask1 = a[an1[i]].second.second;
        size_t mask2 = another->a[an2[i]].second.second;
        
        size_t cross = mask1 ^ mask2;
        
        mask1 = mask1 & (~cross);
        mask2 = mask1 & (~cross);
        
        while (cross != 0)
        {
            size_t mi;
            while ((cross & (1 << (mi = random(xs)))) == 0);
            mask1 = mask1 | (1 << mi);
            cross = cross ^ (1 << mi);
            while ((cross & (1 << (mi = random(xs)))) == 0);
            mask2 = mask2 | (1 << mi);
            cross = cross ^ (1 << mi);
        }
        
        assert(s1 < SM_PARAM(size_t, rt_automaton_size));
        assert(s2 < SM_PARAM(size_t, rt_automaton_size));
        
        a1.push_back(std::make_pair(s1, std::make_pair(c1, mask1))); //FIXME
        a2.push_back(std::make_pair(s2, std::make_pair(c2, mask2))); //FIXME
    }

    return std::make_pair(boost::shared_ptr<individual>(new reduced_table(a1)),
            boost::shared_ptr<individual>(new reduced_table(a2)));
}

size_t reduced_table::get_states_count() const
{
    return SM_PARAM(size_t, rt_automaton_size);
}

size_t reduced_table::input_event(input const & i, output & o, size_t state) const
{
    size_t x = 0;
    size_t xs = SM_PARAM(size_t, rt_x_size);
    size_t rxs = SM_PARAM(size_t, rt_r_x_size);

    size_t ep = 0;
    
    for (size_t j = 0; j < xs; ++j)
    {
        if (a[state].second.second & (1 << j))
        {
            if (i.x(j))
            {
                x = x | (1 << ep);
            }
       
            ++ep;
        }
    }

    size_t ns = a[state].second.first[x];
    o.z(a[ns].first);

    return ns;
}

void reduced_table::draw(const char * filename)
{/*
  digraph G {
    1 -> 2 [label="qq"]
    1 -> 3
    1 -> 1
    2 -> 3
    3 -> 2
}*/
    std::fstream out(filename, std::ios::out);
    out << "digraph G {\n";
    size_t sz = SM_PARAM(size_t, rt_automaton_size);
    size_t xs = SM_PARAM(size_t, rt_r_x_size);
    
    for (size_t i = 0; i < sz; ++i)
    {
        for (size_t j = 0; j < (1 << xs); ++j)
        {
            out << i << " -> " << a[i].second.first[j] << ";\n";
        }
    }
    
    out << "}" << std::flush;
    out.close();
}

}
}
