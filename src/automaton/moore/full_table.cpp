/*
 * full_table.cpp
 *
 *  Created on: Oct 14, 2011
 *      Author: Filchenko Nikolay
 */

#include "automaton/moore/full_table.h"

#include "settings/settings_manager.h"

#include "global.h"
#include <iostream>
#include <fstream>
#include <stack>

SM_ADD_PARAM(size_t, automaton_size, 20)
SM_ADD_PARAM(size_t, x_size, 8)
SM_ADD_PARAM(size_t, z_count, 3)
SM_ADD_PARAM(bool, one_point_crossover, false)
SM_ADD_PARAM(bool, mutate_count, 0)
SM_ADD_PARAM(double, mutate_p, 0.15)


namespace automaton {
namespace moore {

full_table::full_table()
{
    size_t sz = SM_PARAM(size_t, automaton_size);
    size_t xs = SM_PARAM(size_t, x_size);
    size_t zc = SM_PARAM(size_t, z_count);

    for (size_t i = 0; i < sz; ++i)
    {
        a.push_back(std::pair<size_t, std::vector<size_t> >(std::make_pair(0, std::vector<size_t>(1 << xs))));

        a[i].first = random(zc);

        for (size_t j = 0; j < (1 << xs); ++j)
        {
            a[i].second[j] = random(sz);
        }

    }
}

boost::shared_ptr<individual> full_table::mutate() const
{
    return boost::shared_ptr<full_table>(new full_table(this));
}

full_table::full_table(const full_table * o)
{
    a = o->a;
    size_t mc = SM_PARAM(size_t, mutate_count);
    double mp = SM_PARAM(double, mutate_p);
    size_t sz = SM_PARAM(size_t, automaton_size);
    size_t xs = SM_PARAM(size_t, x_size);
    size_t zc = SM_PARAM(size_t, z_count);

    if (mc)
    {
        std::vector<bool> mut(sz, false);
        size_t c = 0;

        while (c < mc)
        {
            size_t mr = random(sz);

            if (!mut[mr])
            {
                mut[mr] = true;
                ++c;
            }
        }

        for (size_t j = 0; j < sz; ++j)
        {
            if (mut[j])
            {
                if (random(0.5))
                {
                    a[j].first = random(zc);
                }
                else
                {
                    for (size_t i = 0; i < (1 << xs); ++i)
                    {
                        if (random(mp))
                        {
                            a[j].second[i] = random(sz);
                        }
                    }
                }
            }
        }
    }
    else
    {
        for (size_t j = 0; j < sz; ++j)
        {
            if (random(mp))
            {
                if (random(0.5))
                {
                    a[j].first = random(zc);
                }
                else
                {
                    for (size_t i = 0; i < (1 << xs); ++i)
                    {
                        if (random(0.5))
                        {
                            a[j].second[i] = random(sz);
                        }
                    }
                }
            }
        }
    }
}

full_table::full_table(std::vector<std::pair<size_t, std::vector<size_t> > > const & a_) :
    a(a_)
{
}

std::pair<boost::shared_ptr<individual>, boost::shared_ptr<individual> > full_table::corssover(boost::shared_ptr<individual> const & o) const
{
    boost::shared_ptr<full_table> another = boost::dynamic_pointer_cast<full_table>(o);

    std::vector<std::pair<size_t, std::vector<size_t> > > a1, a2;
    size_t sz = SM_PARAM(size_t, automaton_size);

    std::vector<size_t> an1(0), an2(0), rn1(sz), rn2(sz);

    std::stack<size_t, std::vector<size_t> > st;
    std::vector<bool> mark(sz, false);
    
    for (size_t i = 0; i < sz; ++i)
    {
        if (mark[i])
            continue;
        st.push(i);
        mark[i] = true;
        while (!st.empty())
        {
            size_t u = st.top();
            st.pop();
            rn1[u] = an1.size();
            an1.push_back(u);

            for (size_t i = 0; i < a[u].second.size(); ++i)
            {
                size_t v = a[u].second[i];
                if (!mark[v])
                {
                    st.push(v);
                    mark[v] = true;
                }
            }
        }
    }
    mark.assign(sz, false);
    for (size_t i = 0; i < sz; ++i)
    {
        if (mark[i])
            continue;
        st.push(i);
        mark[i] = true;
        while (!st.empty())
        {
            size_t u = st.top();
            st.pop();
            rn2[u] = an2.size();
            an2.push_back(u);

            for (size_t i = 0; i < another->a[u].second.size(); ++i)
            {
                size_t v = another->a[u].second[i];
                if (!mark[v])
                {
                    st.push(v);
                    mark[v] = true;
                }
            }
        }
    }
    bool opc = SM_PARAM(bool, one_point_crossover);
    size_t xs = SM_PARAM(size_t, x_size);
    
    for (size_t i = 0; i < sz; ++i)
    {
        size_t fp = opc ? 0 : random(1 << xs);
        size_t sp = random(1 << xs);

        std::vector<size_t> c1, c2;

        if (fp > sp)
            std::swap(fp, sp);

        for (size_t j = 0; j < fp; ++j)
        {
            c1.push_back(rn1[a[an1[i]].second[j]]);
            c2.push_back(rn2[another->a[an2[i]].second[j]]);
        }

        for (size_t j = fp; j < sp; ++j)
        {
            c2.push_back(rn1[a[an1[i]].second[j]]);
            c1.push_back(rn2[another->a[an2[i]].second[j]]);
        }

        for (size_t j = sp; j < (1 << xs); ++j)
        {
            c1.push_back(rn1[a[an1[i]].second[j]]);
            c2.push_back(rn2[another->a[an2[i]].second[j]]);
        }

        size_t s1 = a[an1[i]].first, s2 = another->a[an2[i]].first;

        if (random(0.5))
            std::swap(s1, s2);

        a1.push_back(std::make_pair(s1, c1));
        a2.push_back(std::make_pair(s2, c2));
    }

    return std::make_pair(boost::shared_ptr<individual>(new full_table(a1)),
            boost::shared_ptr<individual>(new full_table(a2)));
}

size_t full_table::get_states_count() const
{
    return SM_PARAM(size_t, automaton_size);
}

size_t full_table::input_event(input const & i, output & o, size_t state) const
{
    size_t x = 0;
    size_t xs = SM_PARAM(size_t, x_size);

    for (size_t j = 0; j < xs; ++j)
    {
        if (i.x(j))
            x = x | (1 << j);
    }

    size_t ns = a[state].second[x];
    o.z(a[ns].first);

    return ns;
}

void full_table::draw(const char * filename)
{/*
  digraph G {
    1 -> 2 [label="qq"]
    1 -> 3
    1 -> 1
    2 -> 3
    3 -> 2
}*/
    std::fstream out(filename, std::ios::out);
    
    size_t sz = SM_PARAM(size_t, automaton_size);
    size_t xs = SM_PARAM(size_t, x_size);
    out << sz << " 0\n";
    for (size_t i = 0; i < sz; ++i)
    {
        out << a[i].first << " ";
    }
    
    out << "\n";
    
    for (size_t i = 0; i < sz; ++i)
    {
        for (size_t j = 0; j < (1 << xs); ++j)
        {
            out << i << " " << j << " " << a[i].second[j] << "\n";
        }
    }
    
    out << std::flush;
    out.close();
}

}
}
