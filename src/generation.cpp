#include "generation.h"
#include <algorithm>
#include <boost/make_shared.hpp>
#include "settings/settings_manager.h"

#include <boost/thread.hpp>

SM_ADD_PARAM(size_t, gsize, 100);
SM_ADD_PARAM(size_t, threads, 8);
SM_ADD_PARAM(double, elite, 0.02);
SM_ADD_PARAM(double, mutation_p, 0.07);
SM_ADD_PARAM(double, mutation_bp, 0.7);

#define N_THREADS 8

bool less(boost::shared_ptr<individual> const & o1, boost::shared_ptr<individual> const & o2)
{
    return (*o1) < (*o2);
}

generation::generation(std::vector<boost::shared_ptr<individual> > const & d) : data(d)
{
}

generation::generation(boost::function<boost::shared_ptr<individual>()> const & factory)
{
    size_t gs = SM_PARAM(size_t, gsize);
    for (size_t i = 0; i < gs; ++i)
    {
        data.push_back(factory());
    }
}

void executor(std::vector<boost::shared_ptr<individual> > const & data, boost::shared_ptr<problem::model> const & model, size_t ipos)
{
        size_t gs = SM_PARAM(size_t, gsize);
        for (size_t i = ipos; i < gs; i += N_THREADS)
        {
            data[i]->fitness = model->fitness(data[i]);
        }
}

boost::shared_ptr<generation> generation::next(boost::shared_ptr<problem::model> const & model, bool bp) const
{
    model->generation();
    size_t gs = SM_PARAM(size_t, gsize);
    size_t threads = SM_PARAM(size_t, threads);

    std::vector<boost::shared_ptr<boost::thread> > workers;
    
    for (size_t i = 0; i < threads; ++i)
    {
        workers.push_back(boost::make_shared<boost::thread>(boost::bind(executor, data, model, i)));
    }
    
    for (size_t i = 0; i < threads; ++i)
    {
        workers[i]->join();
    }
    
    std::sort(data.begin(), data.end(), less);
    
    size_t el = SM_PARAM(double, elite) * gs;
    double pm = SM_PARAM(double, mutation_p);
    double bpm = SM_PARAM(double, mutation_bp);
    
    std::vector<boost::shared_ptr<individual> > nd;
    
    for (size_t i = 0; i < el; ++i)
    {
        nd.push_back(data[i]);
    }
    
    while (nd.size() < gs)
    {
        size_t id1 = 0;
        size_t id2 = 0;
        
        while (!random(0.03))
            id1 = (id1 + 1) % data.size();
        
        while (!random(0.03))
            id2 = (id2 + 1) % data.size();
        
        assert(id1 < data.size());
        assert(id2 < data.size());
        
        std::pair<boost::shared_ptr<individual>,boost::shared_ptr<individual> > ni =
            data[id1]->corssover(data[id2]);
        if (random(bp ? bpm : pm))
        {
            ni.first = ni.first->mutate();
        }
        if (random(bp ? bpm : pm))
        {
            ni.second = ni.second->mutate();
        }
        
        nd.push_back(ni.first);
        nd.push_back(ni.second);
    }
    
    if (nd.size() > gs)
        nd.pop_back();
    
    return boost::shared_ptr<generation>(new generation(nd));
}


double generation::best_fitness() const
{
    return data[0]->fitness;
}

double generation::worst_fitness() const
{
    return data.back()->fitness;
}

double generation::avg_fitness() const
{
    size_t gs = SM_PARAM(size_t, gsize);
    double r = 0;
    for (size_t i = 0; i < gs; ++i)
        r += data[i]->fitness;
    return r / gs;
}

boost::shared_ptr<individual> generation::best_individual()
{
    return data[0];
}

