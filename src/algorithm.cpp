#include "algorithm.h"
#include <iostream>
#include <boost/make_shared.hpp>
#include "settings/settings_manager.h"
#include <fstream>
#include "automaton/representation.h"
SM_ADD_PARAM(size_t, gens, 200);
SM_ADD_PARAM(size_t, gbmut, 70);

algorithm::algorithm(boost::function<boost::shared_ptr<individual>()> const & individual_factory,
             boost::shared_ptr<problem::model> const & mod) : _model(mod), factory_(individual_factory)
{
}

void algorithm::run(const char * filename)
{
    boost::shared_ptr<generation> g(boost::make_shared<generation>(factory_));
    boost::shared_ptr<generation> ng;
    size_t gs = SM_PARAM(size_t, gens);
    size_t gbm = SM_PARAM(size_t, gbmut);
    std::fstream out((std::string(filename) + ".log").c_str(), std::ios::out);
    
    for (size_t i = 0; i < gs; ++i)
    {
        ng = g->next(_model, (i % gbm == 0));
        std::cout << g->best_fitness() << " : " << g->avg_fitness() << std::endl;
        out << g->best_fitness() << " " << g->avg_fitness() << " " << g->worst_fitness() <<  "\n";
        g = ng;
    }
    
    _model->fitness(g->best_individual(), true, filename);
    
    boost::dynamic_pointer_cast<automaton::representation>(g->best_individual())->draw((std::string(filename) + "_best_result.log").c_str());
    
    out << std::flush;
    out.close();
}
