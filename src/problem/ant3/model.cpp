#include "problem/ant3/model.h"
#include "settings/settings_manager.h"

#include "automaton/input.h"
#include "automaton/output.h"
#include "automaton/representation.h"
#include <fstream>
#include <boost/lexical_cast.hpp>


SM_ADD_PARAM(bool, recreate_fields, false);
SM_ADD_PARAM(size_t, fields_count, 20);
SM_ADD_PARAM(size_t, steps, 200);
SM_ADD_PARAM(size_t, field_w, 32);
SM_ADD_PARAM(size_t, field_h, 32);
SM_ADD_PARAM(double, apple_p, 0.05);

namespace problem {
namespace ant3 {

model::model()
{
    reset();
}

class automaton_input : public automaton::input{
private:
    char inp;
public:
    automaton_input(char c) : inp(c) {};
    virtual bool x(size_t i) const
    {
        return inp & (1 << i);
    }
};

class automaton_output : public automaton::output{
public:
    mutable size_t act;
public:
    automaton_output() : act(0) {};
    virtual void z(size_t i) const
    {
        act = i;
    }
};

double model::fitness(boost::shared_ptr<individual> const & ind, bool print, const char * filename)
{
    size_t fc = SM_PARAM(size_t, fields_count);
    size_t fw = SM_PARAM(size_t, field_w);
    size_t fh = SM_PARAM(size_t, field_h);
    size_t sc = SM_PARAM(size_t, steps);
    
    boost::shared_ptr<automaton::representation> aut = boost::dynamic_pointer_cast<automaton::representation>(ind);
    
    double fit = 0;
    
    std::fstream hist;

    if (print)
    {
        hist.open((std::string(filename) + "_fitness.log").c_str(), std::ios::out);
    }
    
    for (size_t f = 0; f < fc; ++f)
    {
        double cf = 0;
        size_t x = 0;
        size_t y = 0;
        size_t d = 0;
        size_t cstate = 0;
        std::set<std::pair<size_t, size_t> > cfield = fields[f].first;
        size_t ls = 0;
        if (print)
            hist << "field" << f << "\n";
        for (size_t step = 0; step < sc; ++step)
        {
            if (print)
                hist << x << " " << y << "\n";
            size_t xa[8], ya[8];
            switch (d) {
                case 0:
                    xa[0] = xa[1] = xa[5] = xa[7] = x;
                    ya[0] = y - 2;
                    ya[1] = y - 1;
                    ya[5] = y + 1;
                    ya[7] = y + 2;
                    xa[2] = xa[3] = xa[6] = x + 1;
                    ya[2] = y - 1;
                    ya[3] = y;
                    ya[6] = y + 1;
                    xa[4] = x + 2;
                    ya[4] = y;
                    break;
                case 1:
                    ya[0] = ya[1] = ya[5] = ya[7] = y;
                    xa[0] = x - 2;
                    xa[1] = x - 1;
                    xa[5] = x + 1;
                    xa[7] = x + 2;
                    ya[2] = ya[3] = ya[6] = y + 1;
                    xa[2] = x - 1;
                    xa[3] = x;
                    xa[6] = x + 1;
                    ya[4] = y + 2;
                    xa[4] = x;
                    break;
                case 2:
                    xa[0] = xa[1] = xa[5] = xa[7] = x;
                    ya[0] = y + 2;
                    ya[1] = y + 1;
                    ya[5] = y - 1;
                    ya[7] = y - 2;
                    xa[2] = xa[3] = xa[6] = x - 1;
                    ya[2] = y + 1;
                    ya[3] = y;
                    ya[6] = y - 1;
                    xa[4] = x - 2;
                    ya[4] = y;
                    break;
                case 3:
                    ya[0] = ya[1] = ya[5] = ya[7] = y;
                    xa[0] = x + 2;
                    xa[1] = x + 1;
                    xa[5] = x - 1;
                    xa[7] = x - 2;
                    ya[2] = ya[3] = ya[6] = y - 1;
                    xa[2] = x + 1;
                    xa[3] = x;
                    xa[6] = x - 1;
                    ya[4] = y - 2;
                    xa[4] = x;
                    break;
            };
            
            char inp = 0;
            
            for (size_t k = 0; k < 8; ++k)
            {
                xa[k] = (xa[k] + fw) % fw;
                ya[k] = (ya[k] + fh) % fh;
                if (cfield.find(std::make_pair(xa[k], ya[k])) != cfield.end())
                {
                    inp = inp | (1 << k);
                }
            }
            
            automaton_input in(inp);
            automaton_output out;
            cstate = aut->input_event(dynamic_cast<automaton::input&>(in),
                dynamic_cast<automaton::output&>(out),
                cstate);
            switch(out.act)
            {
                case 0:
                    switch (d)
                    {
                        case 0:
                            x++;
                            break;
                        case 1:
                            y++;
                            break;
                        case 2:
                            x--;
                            break;
                        case 3:
                            y--;
                            break;
                    };
                    break;
                case 1:
                    d = (d + 1) % 4;
                    break;
                case 2: 
                    d = (d + 3) % 4;
                    break;
            };
            
            x = (x + fw) % fw;
            y = (y + fh) % fh;
            
            if (cfield.find(std::make_pair(x, y)) != cfield.end())
            {
                cf += 1;
                ++ls;
                cfield.erase(std::make_pair(x, y));
            }
                        
        }
        cf *= 100;//fw * fh;
        cf /= fields[f].second;
        if (ls > 0)
        {
            cf += 1.0 / ls;
        }
        fit += cf;
    }
    
    if (print)
    {
        hist << std::flush;
        hist.close();
    }
    
    return fit / fc;
}

void model::generation()
{
    if (SM_PARAM(bool, recreate_fields))
        reset();
}

void model::reset()
{
    size_t fc = SM_PARAM(size_t, fields_count);
    size_t fw = SM_PARAM(size_t, field_w);
    size_t fh = SM_PARAM(size_t, field_h);
    
    fields.clear();
    double ap = SM_PARAM(double, apple_p);
    
    for (size_t f = 0; f < fc; ++f)
    {
        fields.push_back(std::make_pair(std::set<std::pair<size_t, size_t> >(), 0));
        while(fields.back().second == 0)
        {
            for (size_t i = 0; i < fw; ++i)
            {
                for (size_t j = 0; j < fh; ++j)
                {
                    if (random(ap))
                    {
                        fields.back().first.insert(std::make_pair(i, j));
                        ++fields.back().second;
                    }
                }
            }
        }
        
        std::fstream field(("field_" + boost::lexical_cast<std::string>(f)).c_str(), std::ios::out);
        field << fw << " " << fh << "\n";
        for (size_t i = 0; i < fw; ++i)
        {
            for (size_t j = 0; j < fh; ++j)
            {
                field << ((fields.back().first.find(std::make_pair(i, j)) != fields.back().first.end()) ? "@" : ".");
            }
            field << "\n";
        }
        field << std::flush;
        field.close();

    }
}

}
}
