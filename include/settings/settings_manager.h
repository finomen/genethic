/*
 * settings_manager.h
 *
 *  Created on: Oct 14, 2011
 *      Author: Filchenko Nikolay
 */

#ifndef SETTINGS_MANAGER_H_
#define SETTINGS_MANAGER_H_

#include <boost/noncopyable.hpp>

#include <string>
#include <map>
#include <boost/lexical_cast.hpp>

namespace settings {

class settings_manager : private boost::noncopyable
{
public:
    template<typename T>
    T read_value(std::string const & name)
    {
        return boost::lexical_cast<T>(set[name]);
    }
    
    template<typename T>
    void add_value(std::string const & name, T const & def)
    {
        set[name] = boost::lexical_cast<std::string>(def);
    }

    void override(std::string const & name, std::string const & value)
    {
        set[name] = value;
    }

    std::string data()
    {
        std::string res;
        for (std::map<std::string, std::string>::iterator it = set.begin(); it != set.end(); ++it)
        {
            res += it->first + " = " + it->second + "\n";
        }
        
        return res;
    }

    static settings_manager & instance()
    {
        static settings_manager sm;
        return sm;
    }

private:
    std::map<std::string, std::string> set;
};

namespace detail {
template<typename T>
struct __param_declaration
{
    __param_declaration(std::string const & name, T const & def)
    {
        settings_manager::instance().add_value<T>(name, def);
    }
};
}

}

#define SM_ADD_PARAM(type, name, def) \
    settings::detail::__param_declaration<type> sm_param_ ## name(#name, def);

#define SM_PARAM(type, name)\
    (settings::settings_manager::instance().read_value<type>(#name))

/*
#define SM_ADD_PARAM(type, name, def) type name = def;
#define SM_PARAM(type, name) name
*/
#endif /* SETTINGS_MANAGER_H_ */
