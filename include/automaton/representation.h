/*
 * representation.h
 *
 *  Created on: Oct 14, 2011
 *      Author: Filchenko Nikolay
 */

#ifndef REPRESENTATION_H_
#define REPRESENTATION_H_

#include "automaton/input.h"
#include "automaton/output.h"
#include "individual.h"


namespace automaton {

class representation : public individual
{
public:
    virtual size_t get_states_count() const = 0;
    virtual size_t input_event(input const & i, output & o, size_t state) const = 0;
    virtual ~representation() {}
    virtual void draw(const char * filename) = 0;
};

}

#endif /* REPRESENTATION_H_ */
