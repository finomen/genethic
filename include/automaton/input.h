/*
 * input.h
 *
 *  Created on: Oct 14, 2011
 *      Author: Filchenko Nikolay
 */

#ifndef INPUT_H_
#define INPUT_H_

#include <cstring>

namespace automaton {

class input
{
public:
    virtual bool x(size_t i) const = 0;
};

}

#endif /* INPUT_H_ */
