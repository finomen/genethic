/*
 * reduced_table.h
 *
 *  Created on: Oct 14, 2011
 *      Author: Filchenko Nikolay
 */

#ifndef REDUCED_TABLE_H_
#define REDUCED_TABLE_H_

#include "automaton/representation.h"
#include <vector>

namespace automaton {

namespace moore {

class reduced_table : public representation
{
public:
    reduced_table();
    virtual boost::shared_ptr<individual> mutate() const;
    virtual std::pair<boost::shared_ptr<individual>, boost::shared_ptr<individual> > corssover(boost::shared_ptr<individual> const & o) const;
    virtual size_t get_states_count() const;
    virtual size_t input_event(input const & i, output & o, size_t state) const;
    virtual void draw(const char * filename);
private:
    reduced_table(const reduced_table * o);
    reduced_table(std::vector<std::pair<size_t, std::pair<std::vector<size_t>, size_t> > > const & a_);
    std::vector<std::pair<size_t, std::pair<std::vector<size_t>, size_t> > > a;
};

}

}

#endif /* REDUCED_TABLE_H_ */
