/*
 * full_table.h
 *
 *  Created on: Oct 14, 2011
 *      Author: Filchenko Nikolay
 */

#ifndef FULL_TABLE_H_
#define FULL_TABLE_H_

#include "automaton/representation.h"
#include <vector>

namespace automaton {

namespace moore {

class full_table : public representation
{
public:
    full_table();
    virtual boost::shared_ptr<individual> mutate() const;
    virtual std::pair<boost::shared_ptr<individual>, boost::shared_ptr<individual> > corssover(boost::shared_ptr<individual> const & o) const;
    virtual size_t get_states_count() const;
    virtual size_t input_event(input const & i, output & o, size_t state) const;
    virtual void draw(const char * filename);
private:
    full_table(const full_table * o);
    full_table(std::vector<std::pair<size_t, std::vector<size_t> > > const & a_);
    std::vector<std::pair<size_t, std::vector<size_t> > > a;
};

}

}

#endif /* FULL_TABLE_H_ */
