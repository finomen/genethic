/*
 * output.h
 *
 *  Created on: Oct 14, 2011
 *      Author: Filchenko Nikolay
 */

#ifndef OUTPUT_H_
#define OUTPUT_H_

namespace automaton {

#include <cstring>

class output
{
public:
    virtual void z(size_t) const = 0;
};

}

#endif /* OUTPUT_H_ */
