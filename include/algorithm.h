#ifndef _H_ALGORITHM_
#define _H_ALGORITHM_

#include "global.h"
#include "problem/model.h"
#include "individual.h"
#include "generation.h"

class algorithm 
{
public:
    algorithm(boost::function<boost::shared_ptr<individual>()> const & individual_factory,
             boost::shared_ptr<problem::model> const & mod);
    void run(const char * filename);
    
private:
    boost::function<boost::shared_ptr<individual>()> factory_;
    boost::shared_ptr<problem::model> _model;
};

#endif
