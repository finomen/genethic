/*
 * global.h
 *
 *  Created on: Oct 14, 2011
 *      Author: Filchenko Nikolay
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <cmath>
#include <math.h>
#include <time.h>
#include <cstdlib>

inline void rinit()
{
    static bool init = false;
    if (!init)
    {
        srand(time(NULL));
        init = true;
    }
}

inline bool random(double p)
{
    rinit();
    return (static_cast<double>(rand()) / RAND_MAX) <= p;
}

inline size_t random(int max)
{
    return (rand() % max);
}

inline size_t random(size_t max)
{
    return (rand() % max);
}

#endif /* GLOBAL_H_ */
