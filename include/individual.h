/*
 * individual.h
 *
 *  Created on: Oct 14, 2011
 *      Author: Filchenko Nikolay
 */

#ifndef INDIVIDUAL_H_
#define INDIVIDUAL_H_

#include <boost/shared_ptr.hpp>

class individual
{
public:
    virtual ~individual() {};
    virtual boost::shared_ptr<individual> mutate() const = 0;
    virtual std::pair<boost::shared_ptr<individual>, boost::shared_ptr<individual> > corssover(boost::shared_ptr<individual> const & o) const = 0;
    
    mutable double fitness;
    bool operator <(individual const & i)
    {
        return fitness > i.fitness;
    }
};

#endif /* INDIVIDUAL_H_ */
