/*
 * generation.h
 *
 *  Created on: Oct 14, 2011
 *      Author: Filchenko Nikolay
 */

#ifndef GENERATION_H_
#define GENERATION_H_

#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include "individual.h"
#include <vector>
#include "problem/model.h"

class generation
{
public:
    generation(boost::function<boost::shared_ptr<individual>()> const & factory);
    boost::shared_ptr<generation> next(boost::shared_ptr<problem::model> const & model, bool bmut = 0) const;
    double best_fitness() const;
    double avg_fitness() const;
    double worst_fitness() const;

    boost::shared_ptr<individual> best_individual();

private:
    generation(std::vector<boost::shared_ptr<individual> > const &);
    mutable std::vector<boost::shared_ptr<individual> > data;
};

#endif /* GENERATION_H_ */
