#ifndef ANT3_MODEL_H
#define ANT3_MODEL_H

#include "problem/model.h"
#include "individual.h"
#include "automaton/representation.h"
#include <set>
#include <vector>

namespace problem {
namespace ant3 {

class model : public problem::model {
public:
    model();
    virtual double fitness(boost::shared_ptr<individual> const & ind, bool print = false, const char * fname = 0);
    virtual void generation();
private:
    void reset();
    std::vector<std::pair<std::set<std::pair<size_t, size_t> >, size_t> > fields;
};

}
}

#endif //ANT3_MODEL_H
