#ifndef _H_MODEL_
#define _H_MODEL_

#include "global.h"
#include "individual.h"

namespace problem {

class model {
public:
    virtual double fitness(boost::shared_ptr<individual> const & ind, bool print = false, const char * filename = 0) = 0;
    virtual void generation() = 0;
};

}

#endif
