#!/usr/bin/php
<?php
class BarGraph 
{ 
    var $barWidth; 
    var $imgHeight=400; 
    var $imgWidth=600; 
    var $bgColor,$barColor; 
    var $barPadding; 
    var $data,$rangeMax=10; 
    var $im; 

    function init() /* initializes the image */ 
    { 
        $this->im=imagecreate($this->imgWidth,$this->imgHeight); 
    } 

    function setHeightWidth($h,$w) /** sets the hieght and with of the image **/ 
    { 
        $this->imgHeight=$h; 
        $this->imgWidth=$w; 
    } 

    function setBarWidth($width) /* sets the bar width */ 
    { 
        $this->barWidth=$width; 
    } 

    function setBarPadding($padding) /* sets the bar padding */ 
    { 
        $this->barPadding=$padding; 
    } 

    function setMax($max) /* sets the maximum posible value in the data set */ 
    { 
        $this->rangeMax=$max; 
    } 

    function loadData($data) /* load data, the input shud be an array */ 
    { 
        $this->data=$data; 
    } 

    function setBgColor($r,$g,$b) /* sets the background color of the image */ 
    { 
        $this->bgColor=imagecolorallocate($this->im,$r,$g,$b); 
    } 

    function setBarColor($r,$g,$b) /* sets the bar color of the image */ 
    { 
        $this->barColor=imagecolorallocate($this->im,$r,$g,$b); 
    } 

    function drawGraph($flag=0) /* to draw graphs on the image */ 
    { 
        if($flag) /* flag set to 1 to draw the second bar **/ 
        { 
            $t=$this->barWidth+$this->barPadding; 
        } 
        else /* else draws the first bar set */ 
        { 
            imagefilledrectangle($this->im,0,0,$this->imgWidth,$this->imgHeight,$this->bgColor); 
            $t=0; 
        } 

        $lx = 0;
        $ly = $this->imgHeight;

        for ( $mon = 0 ; $mon < count($this->data) ; $mon ++ )  
        { 
            /*
            $X = (($this->imgWidth/count($this->data))*$mon) + $this->barPadding + $t; 
            $Y = (10 - $this->data[$mon])*($this->imgHeight/$this->rangeMax); 
            $X1 = ($X + $this->barWidth); 
            $Y1 = $this->imgHeight; 

            imagefilledrectangle($this->im,$X,$Y,$X1,$Y1,$this->barColor); 
            */
            $x = (($this->imgWidth/count($this->data))*$mon) + $t; 
            $y = ($this->rangeMax - $this->data[$mon])*($this->imgHeight/$this->rangeMax); 
            
            imageline($this->im, $x, $y, $lx, $ly, $this->barColor);
            $lx = $x;
            $ly = $y;
        } 
    } 

    function renderImage($file) /* creates the image & sends in to the browser */ 
    { 
        //header("Content-Type: image/png"); 
        imagepng($this->im, $file); 
    } 
}  

$fileName = "full_table.log";
$fileArr = file($fileName);
$fileArr = array_map('floatval', $fileArr);

$length = count($fileArr);

$pr = array();
$maxVal = 0;
for ($i = 0; $i < $length; $i += 3)
{
    $pr[] = $fileArr[$i];
    if ($fileArr[$i] > $maxVal)
        $maxVal = $fileArr[$i];
    //echo $fileArr[$i] . "\n";
}

$fileName = "reduced_table.log";
$fileArr = file($fileName);
$fileArr = array_map('floatval', $fileArr);

$length = count($fileArr);

$pr1 = array();

for ($i = 0; $i < $length; $i += 3)
{
    $pr1[] = $fileArr[$i];
    if ($fileArr[$i] > $maxVal)
        $maxVal = $fileArr[$i];
    //echo $fileArr[$i] . "\n";
}

$g=new BarGraph; 
$g->setHeightWidth(1000, 1000); 
$g->init(); 
$g->setMax(intval($maxVal) + 1); //maximum data possible in the data set 
$g->setBgColor(204,204,204); 

$g->setBarColor(255,0,0); 
$g->loadData($pr); 
$g->drawGraph(); 

$g->setBarColor(0, 0, 255); 
$g->loadData($pr1); 
$g->drawGraph(1); 


$g->renderImage("plot.png"); 

?>
