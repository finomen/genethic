#!/bin/bash
for i in {1..$1}
do
mkdir run_$i
cd run_$i
../build/src/test
cp ../gen.sh ./
cp ../plot.php ./
./gen.sh
cd ..
done
