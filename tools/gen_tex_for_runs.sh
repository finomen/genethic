#!/bin/bash -e
echo "\subsection{Серия $1}" > part.tex
i=1
apple_p=`cat run_$i/settings.txt | grep "apple_p" | cut -d " " -f 3  | cut -c -5`
automaton_size=`cat run_$i/settings.txt | grep "^automaton_size" | cut -d " " -f 3  | cut -c -5`
elite=`cat run_$i/settings.txt | grep "elite" | cut -d " " -f 3  | cut -c -5`
field_h=`cat run_$i/settings.txt | grep "field_h" | cut -d " " -f 3  | cut -c -5`
field_w=`cat run_$i/settings.txt | grep "field_w" | cut -d " " -f 3  | cut -c -5`
fields_count=`cat run_$i/settings.txt | grep "fields_count" | cut -d " " -f 3  | cut -c -5`
gbmut=`cat run_$i/settings.txt | grep "gbmut" | cut -d " " -f 3  | cut -c -5`
gens=`cat run_$i/settings.txt | grep "gens" | cut -d " " -f 3  | cut -c -5`
gsize=`cat run_$i/settings.txt | grep "gsize" | cut -d " " -f 3  | cut -c -5`
mutate_count=`cat run_$i/settings.txt | grep '^mutate_count' | cut -d " " -f 3  | cut -c -5`
mutate_p=`cat run_$i/settings.txt | grep '^mutate_p' | cut -d " " -f 3  | cut -c -5`
mutation_bp=`cat run_$i/settings.txt | grep "mutation_bp" | cut -d " " -f 3  | cut -c -5`
mutation_p=`cat run_$i/settings.txt | grep "mutation_p" | cut -d " " -f 3  | cut -c -5`
one_point_crossover=`cat run_$i/settings.txt | grep '^one_point_crossover' | cut -d " " -f 3  | cut -c -5`
recreate_fields=`cat run_$i/settings.txt | grep "recreate_fields" | cut -d " " -f 3  | cut -c -5`
rt_automaton_size=`cat run_$i/settings.txt | grep "rt_automaton_size" | cut -d " " -f 3  | cut -c -5`
rt_mutate_count=`cat run_$i/settings.txt | grep "rt_mutate_count" | cut -d " " -f 3  | cut -c -5`
rt_mutate_p=`cat run_$i/settings.txt | grep "rt_mutate_p" | cut -d " " -f 3  | cut -c -5`
rt_one_point_crossover=`cat run_$i/settings.txt | grep "rt_one_point_crossover" | cut -d " " -f 3  | cut -c -5`
rt_r_x_size=`cat run_$i/settings.txt | grep "rt_r_x_size" | cut -d " " -f 3  | cut -c -5`
rt_x_size=`cat run_$i/settings.txt | grep "rt_x_size" | cut -d " " -f 3  | cut -c -5`
rt_z_count=`cat run_$i/settings.txt | grep "rt_z_count" | cut -d " " -f 3  | cut -c -5`
steps=`cat run_$i/settings.txt | grep "steps" | cut -d " " -f 3  | cut -c -5`
x_size=`cat run_$i/settings.txt | grep "x_size" | cut -d " " -f 3  | cut -c -5`
z_count=`cat run_$i/settings.txt | grep "z_count" | cut -d " " -f 3  | cut -c -5`

echo "    \subsection{Настройки}" >> part.tex
echo "        \begin{tabular}{| p{130mm} | c | }" >> part.tex
echo "          \hline                       " >> part.tex
echo "          Вероятность еды в клетке & $apple_p \\\\" >> part.tex
echo "          Ширина поля & $field_w\\\\" >> part.tex
echo "          Высота поля & $field_h\\\\" >> part.tex
echo "          Количество полей & $fields_count\\\\" >> part.tex
echo "          Количесвто шагов & $steps\\\\" >> part.tex
echo "          Количество состояний (полные таблицы) & $automaton_size\\\\" >> part.tex
echo "          Одноточечный кроссовер (полные таблицы) & $one_point_crossover\\\\" >> part.tex
echo "          Вероятность мутации состояния (полные таблицы) & $mutate_p\\\\" >> part.tex
echo "          Количество мутирующий состояний (полные таблицы) & $mutate_count\\\\" >> part.tex
echo "          Количесвто состояний (сокращенные таблицы) & $rt_automaton_size\\\\" >> part.tex
echo "          Одноточечный кроссовер (сокращенные таблицы) & $rt_one_point_crossover\\\\" >> part.tex
echo "          Вероятность мутации состояния (сокращенные таблицы) & $rt_mutate_p\\\\" >> part.tex
echo "          Количество мутирующий состояний (сокращенные таблицы) & $rt_mutate_count\\\\" >> part.tex
echo "          Количество значимых параметров (сокращенные таблицы) & $rt_r_x_size\\\\" >> part.tex
echo "          Размер поколения & $gsize\\\\" >> part.tex
echo "          Количество поколений & $gens\\\\" >> part.tex
echo "          Вероятность мутации & $mutation_p\\\\" >> part.tex
echo "          Количество поколений между большими мутациями & $gbmut\\\\" >> part.tex
echo "          Вероятность большой мутации & $mutation_bp \\\\" >> part.tex
echo "          " >> part.tex
echo "          \hline  " >> part.tex
echo "        \end{tabular}" >> part.tex


../../tools/average.php > average.log
../../tools/gplot_sumplot.gp

echo "    \subsubsection{Усреднение}" >> part.tex
echo "        \begin{figure}[h]" >> part.tex
echo "        \vspace {-5mm}" >> part.tex
echo "        \includegraphics[width=150mm]{../runs/"$1"/average.png}" >> part.tex
echo "        \vspace {-4mm}" >> part.tex
echo "        \caption{Усреднение по серии запусков} " >> part.tex
echo "        \label{Summary_"$1"}" >> part.tex
echo "        \end{figure}" >> part.tex
echo "" >> part.tex

echo "        \newpage" >> part.tex
#echo "    \subsection{Запуски}" >> part.tex
for i in {1..10}
do
../../tools/plot.sh run_$i
<<COMMENT1
echo "    \subsubsection{Запуск $i}" >> part.tex
echo "        \begin{figure}[h]" >> part.tex
echo "        \vspace {-5mm}" >> part.tex
echo "        \includegraphics[width=150mm]{../runs/"$1"/run_$i/full_table.png}" >> part.tex
echo "        \vspace {-4mm}" >> part.tex
echo "        \caption{Полные таблицы} " >> part.tex
echo "        \end{figure}" >> part.tex
echo "" >> part.tex
echo "        \begin{figure}[h]" >> part.tex
echo "        \vspace {-7mm}" >> part.tex
echo "        \includegraphics[width=150mm]{../runs/"$1"/run_$i/reduced_table.png}" >> part.tex
echo "        \vspace {-4mm}" >> part.tex
echo "        \caption{Сокращенные таблицы} " >> part.tex
echo "        \end{figure}" >> part.tex
echo "" >> part.tex
echo "        \begin{figure}[h]" >> part.tex
echo "        \vspace {-7mm}" >> part.tex
echo "        \includegraphics[width=150mm]{../runs/"$1"/run_$i/summary.png}" >> part.tex
echo "        \vspace {-4mm}" >> part.tex
echo "        \caption{Сравнение полных и сокращенных таблиц} " >> part.tex
echo "        \label{RunCompare_"$1"_"$i"}" >> part.tex
echo "        \vspace {-20mm}" >> part.tex
echo "        \end{figure}" >> part.tex
echo "        \newpage" >> part.tex
echo "" >> part.tex
COMMENT1
done
