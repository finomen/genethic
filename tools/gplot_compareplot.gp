#!/usr/bin/gnuplot
reset
set terminal png interlace giant size 800,300 x000000 xDDDDDD
set output ".plot.png"
set encoding utf8

#set xdata time
set xlabel "Generation"

set ylabel "Fitness"
set yrange [0:50]

set key reverse Left inside bottom
set grid

set style line 1 lt 1 lw 2 pt 0 linecolor rgb "red"
set style line 2 lt 1 lw 1 pt 0 linecolor rgb "green"
set style line 3 lt 1 lw 1 pt 0 linecolor rgb "yellow"
set style line 4 lt 1 lw 1 pt 0 linecolor rgb "blue"

set style data linespoints

set multiplot
plot ".plot1.tmp" u 1 title "Full table" linestyle 2, \
"" u 1 title "Full table (bezier)" smooth bezier linestyle 1, \
".plot2.tmp" u 1 title "Reduced table" linestyle 4, \
"" u 1 title "Reduced table (bezier)" smooth bezier  linestyle 3
unset multiplot
