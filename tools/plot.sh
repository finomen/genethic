#!/bin/bash -e
#awk '{print $1}' $1 > .plot.tmp
for t in full_table reduced_table
do
cp $1/$t.log .plot.tmp
../../tools/gplot_runplot.gp
mv .plot.png $1/$t.png
done

cp $1/full_table.log .plot1.tmp
cp $1/reduced_table.log .plot2.tmp
../../tools/gplot_compareplot.gp
rm .plot1.tmp
rm .plot2.tmp
mv .plot.png $1/summary.png

#cd $1
#cp ../run_template.tex ./
#texi2pdf run_template.tex
#cd ..
