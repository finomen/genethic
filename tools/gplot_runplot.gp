#!/usr/bin/gnuplot
reset
set terminal png interlace giant size 800,300 x000000 xDDDDDD
set output ".plot.png"
set encoding utf8

#set xdata time
set xlabel "Generation"

set ylabel "Fitness"
set yrange [0:50]

set key reverse Left inside bottom
set grid

set style line 1 lt 1 lw 2 pt 0 linecolor rgb "red"
set style line 2 lt 1 lw 1 pt 0 linecolor rgb "green"
set style line 3 lt 1 lw 1 pt 0 linecolor rgb "blue"
#set style line 4 lt 1 lw 1 pt 0 linecolor rgb "#FA0AFF"

set style data linespoints

set multiplot
plot ".plot.tmp" u 1 title "best" linestyle 2, \
"" u 1 title "best (bezier)" smooth bezier linestyle 1, \
"" u 2 title "average" linestyle 3
unset multiplot
#"" u 3 title "worst"  linestyle 4