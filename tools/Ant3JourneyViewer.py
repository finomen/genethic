#! /usr/bin/env python

import os
import sys
import string

ORIENT_RIGHT = 0
ORIENT_DOWN = 1
ORIENT_LEFT = 2
ORIENT_UP = 3

ACT_GO_FORWARD = 0
ACT_TURN_LEFT = 2
ACT_TURN_RIGHT = 1
ACT_DO_NOTHING = 4

class Field(object):
    def __init__(self, fileName):
        self.allFoodCount = 0
        with open(fileName, "r") as inp:
            [self.width, self.height] = [int(x) for x in inp.readline().split(" ")[0:2]]
            self.data = []
            for y in xrange(self.height):
                s = inp.readline()
                row = []
                for x in xrange(self.width):
                    if s[x] == '@':
                        row.append(1)
                        self.allFoodCount += 1
                    else:
                        row.append(0)
                self.data.append(row)
    
    def getElement(self, x, y):
        x = (x + self.width) % self.width
        y = (y + self.height) % self.height
        return bool(self.data[y][x] == 1)
    
    def eatElement(self, x, y):
        x = (x + self.width) % self.width
        y = (y + self.height) % self.height
        self.data[y][x] = 2
    
    def getElementForDrawing(self, x, y):
        return self.data[y][x]
    
    def getStartFoodCount(self):
        return self.allFoodCount

class Ant3(object):
    def __init__(self, fileName):
        with open(fileName, "r") as inp:
            [self.n, self.initialState] = [int(x) for x in inp.readline().split(" ")[0:2]]
            
            actionsAsText = inp.readline().split(" ")[0:self.n]
            self.actions = [int(x) for x in actionsAsText]
            assert len(self.actions) == self.n, "Incorrent number of actions (should be equal to number of states)"
            
            self.transitions = {}
            for i in xrange(self.n * 256):
                [state, cellsMask, targetState] = [int(x) for x in inp.readline().split(" ")[0:3]]
                self.transitions[(state, cellsMask)] = targetState
    
    def getTransition(self, state, cellsMask):
        if (state, cellsMask) in self.transitions:
            return self.transitions[(state, cellsMask)]
        else:
            return -1
    
    def getAction(self, state):
        return self.actions[state]
    
    def getInitialState(self):
        return self.initialState
    
    def getStatesCount(self):
        return self.n

import OpenGL
from OpenGL.GL import *
from OpenGL.GLUT import *

DEFAULT_WINDOW_SIZE_X = 800
DEFAULT_WINDOW_SIZE_Y = 600
DEFAULT_WINDOW_POS_X = 100
DEFAULT_WINDOW_POS_Y = 50

FIELD_RECT_X1 = -0.95
FIELD_RECT_Y1 = -0.95
FIELD_RECT_X2 = 0.45
FIELD_RECT_Y2 = 0.95

def middlePoint(p1, p2):
    return [(p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2]

def getCellCoords(i, j, cellXSize, cellYSize):
    return [[cellXSize * i + FIELD_RECT_X1,       cellYSize * j + FIELD_RECT_Y2], 
            [cellXSize * (i + 1) + FIELD_RECT_X1, cellYSize * j + FIELD_RECT_Y2],
            [cellXSize * (i + 1) + FIELD_RECT_X1, cellYSize * (j + 1) + FIELD_RECT_Y2],
            [cellXSize * i + FIELD_RECT_X1,       cellYSize * (j + 1) + FIELD_RECT_Y2]]

def drawQuadCell(i, j, cellXSize, cellYSize):
    cellCoords = getCellCoords(i, j, cellXSize, cellYSize)
    for x in xrange(4):
        glVertex2d(cellCoords[x][0], cellCoords[x][1])

def drawQuadPathMarker(i, j, cellXSize, cellYSize):
    cellCoords = getCellCoords(i, j, cellXSize, cellYSize)
    glVertex2d(cellCoords[0][0] + cellXSize / 3, cellCoords[0][1] + cellYSize / 3)
    glVertex2d(cellCoords[1][0] - cellXSize / 3, cellCoords[1][1] + cellYSize / 3)
    glVertex2d(cellCoords[2][0] - cellXSize / 3, cellCoords[2][1] - cellYSize / 3)
    glVertex2d(cellCoords[3][0] + cellXSize / 3, cellCoords[3][1] - cellYSize / 3)
    
def drawAnt(cellXSize, cellYSize):
    global curAntX
    global curAntY
    global curAntOrient
    
    cellCoords = getCellCoords(curAntX, curAntY, cellXSize, cellYSize)
    
    v1 = cellCoords[curAntOrient % 4]  #really, mod is not nesessary here
    v2 = middlePoint(cellCoords[(curAntOrient + 1) % 4], cellCoords[(curAntOrient + 2) % 4])
    v3 = cellCoords[(curAntOrient + 3) % 4]
    
    glBegin(GL_TRIANGLES)
    glColor3d(0, 0, 0)
    glVertex2d(v1[0], v1[1])
    glVertex2d(v2[0], v2[1])
    glVertex2d(v3[0], v3[1])
    glEnd()

def drawTextXY(x, y, font, text):
    glRasterPos2d(x, y)
    for i in xrange(len(text)):
        glutBitmapCharacter(font, ord(text[i]))

def forwardCell():
    global field
    global curAntX
    global curAntY
    global curAntOrient
    
    fwX = curAntX
    fwY = curAntY
    
    if curAntOrient == ORIENT_RIGHT:
        fwX += 1
    elif curAntOrient == ORIENT_LEFT:
        fwX -= 1
    elif curAntOrient == ORIENT_UP:
        fwY -= 1
    elif curAntOrient == ORIENT_DOWN:
        fwY += 1
    
    fwX = (fwX + field.width) % field.width
    fwY = (fwY + field.height) % field.height
    return [fwX, fwY]

def getEightForwardCells():
    global curAntX
    global curAntY
    global curAntOrient
    
    l = []
    for i in xrange(8):
        l.append([curAntX, curAntY])
    
    if curAntOrient == ORIENT_RIGHT:
        l[0][1] -= 2
        l[1][1] -= 1
        l[2][0] += 1
        l[2][1] -= 1
        l[3][0] += 1
        l[4][0] += 2
        l[5][1] += 1
        l[6][0] += 1
        l[6][1] += 1
        l[7][1] += 2
    elif curAntOrient == ORIENT_LEFT:
        l[0][1] += 2
        l[1][1] += 1
        l[2][0] -= 1
        l[2][1] += 1
        l[3][0] -= 1
        l[4][0] -= 2
        l[5][1] -= 1
        l[6][0] -= 1
        l[6][1] -= 1
        l[7][1] -= 2
    elif curAntOrient == ORIENT_UP:
        l[0][0] -= 2
        l[1][0] -= 1
        l[2][0] -= 1
        l[2][1] -= 1
        l[3][1] -= 1
        l[4][1] -= 2
        l[5][0] += 1
        l[6][0] += 1
        l[6][1] -= 1
        l[7][0] += 2
    elif curAntOrient == ORIENT_DOWN:
        l[0][0] += 2
        l[1][0] += 1
        l[2][0] += 1
        l[2][1] += 1
        l[3][1] += 1
        l[4][1] += 2
        l[5][0] -= 1
        l[6][0] -= 1
        l[6][1] += 1
        l[7][0] -= 2
    
    return l

def makeStep():
    global field
    global ant3
    global steps
    global curAntX
    global curAntY
    global curAntOrient
    global curAntState
    global antPath
    global eaten
    global stepWhereLastFoodEaten
    global fitness
    
    steps += 1
    
    l = getEightForwardCells()
    cellsMask = 0
    for i in xrange(len(l)):
        if field.getElement(l[i][0], l[i][1]):
            cellsMask += 2 ** i
    
    newAntState = ant3.getTransition(curAntState, cellsMask)
    assert newAntState >= 0, "Transition from state {0} by cells mask {1} was not set in input".format(curAntState, cellsMask)
    curAntState = newAntState
    
    antAction = ant3.getAction(curAntState)
    
    if antAction == ACT_GO_FORWARD:
        [curAntX, curAntY] = forwardCell()
        antPath.append([curAntX, curAntY])
        if field.getElement(curAntX, curAntY):
            field.eatElement(curAntX, curAntY)
            eaten += 1
            stepWhereLastFoodEaten = steps  #check this! may be steps - 1, or steps + 1
    elif antAction == ACT_TURN_LEFT:
        curAntOrient = (curAntOrient + 4 - 1) % 4
    elif antAction == ACT_TURN_RIGHT:
        curAntOrient = (curAntOrient + 1) % 4
    
    fitness = eaten * 100.0 / field.getStartFoodCount() + 1.0 / stepWhereLastFoodEaten
    
    glutPostRedisplay()

def drawControlsHelp():
    glColor3d(0, 0, 0)
    drawTextXY(0.55, -0.73, GLUT_BITMAP_HELVETICA_12, "SPACE - make a step")
    drawTextXY(0.55, -0.78, GLUT_BITMAP_HELVETICA_12, "R         - restart")
    drawTextXY(0.55, -0.83, GLUT_BITMAP_HELVETICA_12, "F         - toggle fullscreen")
    drawTextXY(0.55, -0.88, GLUT_BITMAP_HELVETICA_12, "H         - show/hide controls")
    drawTextXY(0.55, -0.93, GLUT_BITMAP_HELVETICA_12, "Esc      - exit")

def displayFunc():
    global field
    global antPath
    global hideControls
    global eaten
    global fitness
    
    cellXSize = (FIELD_RECT_X2 - FIELD_RECT_X1) / field.width
    cellYSize = (FIELD_RECT_Y1 - FIELD_RECT_Y2) / field.height
    
    glClear(GL_COLOR_BUFFER_BIT)
    
    glBegin(GL_QUADS)
    
    glColor3d(0, 1, 0)
    glVertex2d(FIELD_RECT_X1, FIELD_RECT_Y1)
    glVertex2d(FIELD_RECT_X1, FIELD_RECT_Y2)
    glVertex2d(FIELD_RECT_X2, FIELD_RECT_Y2)
    glVertex2d(FIELD_RECT_X2, FIELD_RECT_Y1)
    
    #field
    for i in xrange(field.width):
        for j in xrange(field.height):
            el = field.getElementForDrawing(i, j)
            if el == 1:
                glColor3d(1, 0, 0)
                drawQuadCell(i, j, cellXSize, cellYSize)
            elif el == 2:
                glColor4d(1, 0, 0, 0.2)  #color of eaten food
                drawQuadCell(i, j, cellXSize, cellYSize)
    
    #ant fov
    l = getEightForwardCells()
    glColor4d(0, 0, 1, 0.3)  #color of ant fov
    for cell in l:
        x = (cell[0] + field.width) % field.width
        y = (cell[1] + field.height) % field.height
        drawQuadCell(x, y, cellXSize, cellYSize)
    
    #ant path
    glColor4d(0, 0, 1, 0.33)
    for i in xrange(len(antPath)):
        drawQuadPathMarker(antPath[i][0], antPath[i][1], cellXSize, cellYSize)
    
    glEnd()
    
    glColor3d(0, 0, 0.5)
    drawTextXY(0.6, 0.8, GLUT_BITMAP_HELVETICA_18, "Steps: " + str(steps))
    drawTextXY(0.6, 0.7, GLUT_BITMAP_HELVETICA_18, "Eaten: " + str(eaten))
    drawTextXY(0.58, 0.6, GLUT_BITMAP_HELVETICA_18, "Fitness: " + str(float(fitness))[0:5])
    
    drawAnt(cellXSize, cellYSize)
    if not hideControls:
        drawControlsHelp()
    
    glFlush()

def restart():
    global ant3
    global steps
    global curAntX
    global curAntY
    global curAntOrient
    global curAntState
    global antPath
    global eaten
    
    curAntX = 0
    curAntY = 0
    curAntOrient = ORIENT_RIGHT
    curAntState = ant3.getInitialState()
    steps = 0
    antPath = [[curAntX, curAntY]]
    eaten = 0
    
    glutPostRedisplay()

def keyboardFunc(key, x, y):
    if key == chr(27):
        sys.exit(0)
    elif key == ' ':
        makeStep()
    elif key == 'f' or key == 'F':
        global fullscreenMode
        if fullscreenMode:
            glutReshapeWindow(DEFAULT_WINDOW_SIZE_X, DEFAULT_WINDOW_SIZE_Y)
            glutPositionWindow(DEFAULT_WINDOW_POS_X, DEFAULT_WINDOW_POS_Y)
        else:
            glutFullScreen()
        fullscreenMode = not fullscreenMode
    elif key == 'r' or key == 'R':
        restart()
    elif key == 'h' or key == 'H':
        global hideControls
        hideControls = not hideControls
        glutPostRedisplay()

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: Ant3JourneyViewer.py <field_file_name> <automata_file_name>"
        sys.exit(0)

    if not os.path.isfile(sys.argv[1]):
        print "Error: Field file does not exist"
        sys.exit(1)
        
    if not os.path.isfile(sys.argv[2]):
        print "Error: Ant file does not exist"
        sys.exit(1)

    field = Field(sys.argv[1])
    ant3 = Ant3(sys.argv[2])
    
    curAntX = 0
    curAntY = 0
    curAntOrient = ORIENT_RIGHT
    curAntState = ant3.getInitialState()
    steps = 0
    antPath = [[curAntX, curAntY]]
    eaten = 0
    stepWhereLastFoodEaten = 1
    fitness = 0.0
    
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE)
    glutInitWindowSize(DEFAULT_WINDOW_SIZE_X, DEFAULT_WINDOW_SIZE_Y)
    glutInitWindowPosition(DEFAULT_WINDOW_POS_X, DEFAULT_WINDOW_POS_Y)
    win = glutCreateWindow("Ant3 Journey Viewer")
    
    fullscreenMode = False
    hideControls = False
    
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    
    glutDisplayFunc(displayFunc)
    glutKeyboardFunc(keyboardFunc)
    
    glClearColor(1, 1, 1, 0)
    glutMainLoop()
