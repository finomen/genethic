#!/bin/bash -e
for i in {1..3}
do
cd runs/$i
../../tools/gen_tex_for_runs.sh $i
cd ../..
done